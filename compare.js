/* This file contains all the functions index.html uses */

/* Displays an alert box once the ? button is clicked. */
function displayAbout() {
    var symbol = " ~ ";
    alert("Compares the left box (original text) to the right box (new text).\nWhen comparing, any character you see means that's a different character than the original, and the \"" + symbol + "\" represents same characters.");
}

/* First checks if both text boxes have text.
If so, then this function will call the compareText() function */
function doesTextExist() {
   var left = document.getElementById("leftbox");
   var right = document.getElementById("rightbox");

   if (left.value != "" && right.value != "") {
       compareText(left, right);
   }
   else if (left.value == "" && right.value != "") {
       alert("Put text in the left box before comparing");
   }
   else if (left.value != "" && right.value == "") {
       alert("Put text in the right box before comparing");
   }
   else {
       alert("Put text in both boxes before comparing");
   }
}


function concatinateTildaToResult(word) {
    // console.log("word: " + word);
    var result = "";
    for (var i = 0; i < word.length; i++) {
        result += "~";
    }
    return result;
}

function checkForSpace(left, right) {
    var check = "false";
    for (var i = 0; i < left.length; i++) {
        if (left[i] == ' ' && left[i+1] != "") { // If there's more than one word
            check = "true";
            break;
        }
    }
    
    if (check == "true") { // If the left array has at least two words, check right array now
        for (var j = 0; j < right.length; j++) {            
            if (right[j] == ' ' && right[j+1] != "") {
                return "true";
            }
        }
    }

    return "false";
}


/* Checks if the left side has more text than the right
    @return: -1 if they're the same size
              0 if left has more
              1 if right has more
*/
function isLeftBiggerThanRight(left, right) {
    var isLeftBigger = "";
    if (left.length > right.length) { // Left has more
        isLeftBigger = "true";
    }
    else if (left.length < right.length) { // Right has more
        isLeftBigger = "false";
    }
    else { // Of the same length
        isLeftBigger = "equal";
    }
    return isLeftBigger;
}

/* Compare the two if they have the same length */
function compareBothNoSpaces(left, right) {
    var result = "";
    for (var i = 0; i < left.length; i++) {
        if (left[i] == ' ') {
            result += left[i];
        }
        else if (left[i] != right[i]) {
            result += left[i];
        }
        else {
            result += "~";
        }

    }
    return result;
}

/* Compare the two if left has more text length */
function compareLeftNoSpaces(left, right) {
    var output = "";
    var rightMax = right.length - 1;
    for (var i = 0; i < left.length; i++) {   
        if (left[i] == right[i] && i <= rightMax) {
            output += "~";
        }
        else if (left[i] == right[i-1] && i <= rightMax) { 
            output += "~";
        }
        else if (left[i] == right[rightMax] && i >= rightMax) { 
            output += "~";
        }        
        else {
            output += left[i];
        }
    }

    return output;
}

/* Compare the two if right has more text length */
function compareRightNoSpaces(left, right) {
    var output = "";
    var leftMax = left.length - 1;

    for (var i = 0; i < right.length; i++) {
        if (right[i] == left[i] && i <= leftMax) {
            output += "~";
        }
        else if (right[i] == left[i-1] && i <= leftMax) { 
            output += "~";
        }
        else if (right[i] == left[leftMax] && i >= leftMax) { 
            output += "~";
        }        
        else {
            output += right[i];
        }
    }

    return output;

}


/* These functions are for text that contains spaces */

function compareEqualWithSpaces(left, right) {
    var output = "";
    for (var i = 0; i < left.length; i++) {        
        if (left[i] == right[i]) {
            output += concatinateTildaToResult(left[i]);
        }
        else { //if (left[i] != right[i] && left[i] != right[i-1]) {
            output += left[i];
        }
        
        // Add a space if it's not the last word.
        if (i != left.length - 1) {
            output += " "; 
        }
    }

    return output;
}

function compareLeftWithSpaces(left, right) {
    var output = "";
    var rightMax = right.length - 1;
    for (var i = 0; i < left.length; i++) {        
        if (left[i] == right[i]) {
            output += concatinateTildaToResult(left[i]);
        }
        else if (i == (left.length - 1) && left[i] != right[rightMax-1] && i > rightMax) {
            output += left[i];
        }
        else if (i == (left.length - 1) && left[i] != right[rightMax-1] && i <= rightMax) { // Checks if last word isn't the same
            output += right[rightMax-1];
        }
        else if (left[i] != right[i] && left[i] == right[rightMax-1]) { // Checks if the next word is the same
            output += concatinateTildaToResult(left[i]); // Put ~ for the word that matches
        }
        else { //if (left[i] != right[i] && left[i] != right[i-1]) {
            output += left[i];
        }
        
        // Add a space if it's not the last word.
        if (i != left.length - 1) {
            output += " "; 
        }
    }

    return output;
}

function compareRightWithSpaces(left, right) {
    var output = "";
    var leftMax = left.length - 1;
    for (var i = 0; i < right.length; i++) {        
        if (left[i] == right[i]) {
            output += concatinateTildaToResult(right[i]);
        }
        else if (i == (right.length - 1) && right[i] != left[leftMax-1] && i > leftMax) {
            output += right[i];
        }
        else if (i == (right.length - 1) && right[i] != left[leftMax-1] && i <= leftMax) { // Checks if last word isn't the same
            output += left[leftMax-1];
        }
        else if (right[i] != left[i] && right[i] == left[i-1]) { // Checks if the next word is the same
            output += concatinateTildaToResult(right[i]); // Put ~ for the word that matches
        }
        else { //if (right[i] != left[i] && right[i] != left[i-1]) {
            output += right[i];
        }
        
        // Add a space if it's not the last word.
        if (i != right.length - 1) {
            output += " "; 
        }
    }

    return output;
}

/* Compares the text and prints out what isn't the same */
function compareText(left, right) {

    // Grab every character in the textarea and put them into an array
    var leftChar = left.value.split('');
    var rightChar = right.value.split('');
    var lArray = [];
    var rArray = [];

    // Determine which has a bigger lengtht
    var biggerLength = 0;
    var isLeftBigger = isLeftBiggerThanRight(left.value, right.value);

    var doesSpaceExist = checkForSpace(leftChar, rightChar);
    if (doesSpaceExist == "true") {
        lArray = left.value.split(" ");
        rArray = right.value.split(" ");
    }

    var result = "";
    /* Finds out what's different - SAME LENGTH*/
    if (isLeftBigger == "equal") { 
        if (doesSpaceExist == "true") { result = compareEqualWithSpaces(lArray, rArray); }
        else { result = compareBothNoSpaces(leftChar, rightChar); }
    }

    /* Finds out what's different - LEFT IS LONGER */
    else if (isLeftBigger == "true") { 
        if (doesSpaceExist == "true") { result = compareLeftWithSpaces(lArray, rArray); }
        else { result = compareLeftNoSpaces(leftChar, rightChar); }
    }

    /* Finds out what's different - RIGHT IS LONGER */
    else if (isLeftBigger == "false") { 
        if (doesSpaceExist == "true") { result = compareRightWithSpaces(lArray, rArray); }
        else { result = compareRightNoSpaces(leftChar, rightChar); }
        
    }

    alert("Original:\n" + left.value + "\nNew:\n" + right.value + "\n\nDifference:\n" + result);
    left.value = "";
    right.value = "";
} 
